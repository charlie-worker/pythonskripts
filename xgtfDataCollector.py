# -*- coding: utf-8 -*-
#import cv2
import os
import argparse
import time
import xml.etree.cElementTree as ET

"""Скрипт проходит по папке и вложенным подпапкам,
создавая текстовый файл, в котором:
    дата (отсортировать по возрастанию),
    имя xgtf, сколько в ней объектов,
    длительность видео hh:mm:ss
    И в конце итог сколько всего разметок объектов и времени."""

#Давай сразу договоримся, что подобные утилитки могут
#использоваться как самостоятельные скрипты, так и в качестве модулей
#По этому раздели код: предусмотри метод, 
#кoторый можно позвать из модуля, и код который надо
#запустить если скрип запущен как /отделая программа/. (см if __name__ == '__main__'....) 

#сделал
#а почему у ф-й имена начинаются с __ ?
#при использовании скрипта как модуля лучше не делать их доступными,
def __getXGTFs(dirWithFiles, allovingRec):
    supportExts = ('.xgtf')
    noSortedList = []
    totalTime = 0
    totalObjects = 0
    for name in os.listdir(dirWithFiles):
        path = os.path.join(dirWithFiles, name)
        if os.path.isdir(path) and allovingRec: #не легче рекурсивно получить пути, а уже потом не заморачивась собрать данные?
            #make extra records for nested dir
            extList, extTime, extObj = __getXGTFs(os.path.join(dirWithFiles, name), allovingRec)
            noSortedList.extend(extList)
            totalTime += extTime
            totalObjects += extObj
        elif name.lower().endswith(supportExts):
            dateOfLastCenge = os.path.getctime(path)
            numOfObj, videoTime, videoTimeInSec = __getXGTFdata(ET.ElementTree(file=path))
            totalTime += videoTimeInSec
            totalObjects += numOfObj
            record = (name, str(numOfObj),
                videoTime,
                __convertTime(dateOfLastCenge),
                dateOfLastCenge)
            noSortedList.append(record)
    return (noSortedList, totalTime, totalObjects)

def __getXGTFdata(tree):
    nameStart = '{http://lamp.cfar.umd.edu/viper#}'
    root = tree.getroot()
    objectList = root.findall('.//' + nameStart + 'object')
    #здесь можно наблюдать прекрасную картину того, что ET отказался 
    #кушать сумму двух стрингов, ему подавай один
    #dataNameStart = '{http://lamp.cfar.umd.edu/viperdata#}'
    #framerate = root.find('.//' + dataNameStart + 'fvalue')
    #к нашему сожелению работаь не будет 
    #спасите меня от этих странных ошибок
    #не верю выше написанному)
    framerate = root.find('.//' + '{http://lamp.cfar.umd.edu/viperdata#}fvalue')
    FR = float(framerate.get('value'))
    numframes = root.find('.//' + '{http://lamp.cfar.umd.edu/viperdata#}dvalue')
    NF = float(numframes.get('value'))
    return (len(objectList),
        __secondsToHumanTime(NF / FR),
        NF / FR)

def __secondsToHumanTime(long): 
    return(
        ':'.join(
            list(
                map(
                    lambda x: str(int(x)),
                    [
                        long // (60*60),
                        (long % (60*60)) // 60,
                        long % 60
                    ]
                )
            )
        )
    )

def __convertTime(timeInSec): #do not try understend it
    timeObject = time.localtime(timeInSec)
    return(
        ''.join(
            list(
                map(
                    lambda x: str(x[0] + x[1]),
                    zip(
                        map(
                            str,
                            [
                                timeObject.tm_year,
                                timeObject.tm_mon,
                                timeObject.tm_mday,
                                timeObject.tm_hour,
                                timeObject.tm_min 
                            ]
                        ),
                        (
                            ' year: ',
                            ' month: ',
                            ' day: ',
                            ' hour: ',
                            ' minutes'
                        )
                    )
                )
            )
        )
    )

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Collecting work information')
    parser.add_argument('dirWithFiles', type=str, help='Input dir for .xgtf files', default= '/home/charlie/xgtf/')
    parser.add_argument('--allovingRec', type=bool, help='Check dirs recursively', default= True)
    args = parser.parse_args()
    file = open('resultOfXgtfs', 'w')

    recordsList, time, obj = __getXGTFs(args.dirWithFiles, args.allovingRec)
    recordsList.sort(key=lambda x: x[4])
    file.write('Имена\tКоличество объектов\tВремя видео\tПоследняя редакция\tВсего объектов\t'
                    + str(obj) + '\tВсего времени\t' + __secondsToHumanTime(time) + '\n')
    for record in recordsList:
        file.write(record[0] + '\t' + record[1] + '\t' + record[2] + '\t' + record[3] + '\n')  #используй .format() или подстановки
    file.close()

    #вместо file = open(...) .... file.close() пожно использовать конструкцию вида:
    #with open() as f:
        #код который будет выполняться, если удалось открыть файл

def getData(dirWithFiles, allovingRec):
    recordsList, time, obj = __getXGTFs(dirWithFiles, allovingRec)
    recordsList.sort(key=lambda x: x[4])
    return (recordsList, time, obj)