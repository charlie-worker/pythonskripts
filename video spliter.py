//добавь, пожалуйста, текст задачи

import cv2
import os
from sys import argv  //не используется
import argparse

parser = argparse.ArgumentParser(description='Videos to images')
parser.add_argument('dirWithVideo', type=str, help='Input dir for videos')
parser.add_argument('dir2saveImages', type=str, help='Output dir for image')
parser.add_argument(
    '-n', type=int,
    default=1,
    help='Number of frames per minute(default 1)'
    )
args = parser.parse_args()

def getPath2Video(dirWithVideo):
    supportExts = ('.avi', '.mkv', '.mp4', '.mpeg', '.mov')
    videoList = []
    for name in os.listdir(dirWithVideo):
        path = os.path.join(dirWithVideo, name)
        if name.lower().endswith(supportExts):
            videoList.append(path)
        elif os.path.isdir(path):
            extraList = getPath2Video(path)
            videoList += extraList
    return(videoList)

videosList = getPath2Video(args.dirWithVideo)
countOfVideos = len(videosList)

os.makedirs(args.dir2saveImages, exist_ok=True)
if not os.path.isdir(args.dir2saveImages):
    print("unable to create the resulting folder: {} Abort!".format(args.dir2saveImages))
    exit(-1)

for videoNum, path2video in enumerate(videosList):
    print("[{0}/{1}] Work with video: '{2}'".format(videoNum, countOfVideos, path2video))
    videoName = os.path.basename(path2video)
    video = cv2.VideoCapture(path2video)
    if not video.isOpened():
        print("Unable to open the video: "+videoName)
        continue

    nameOfVideoWithoutExt = videoName.rsplit('.', 1)[0].strip()
    (major_ver, _, _) = (cv2.__version__).split('.')   //мы уже не используем питон 2, можно смело использовать под 3,6+

    fps = video.get(cv2.cv.CV_CAP_PROP_FPS) if int(major_ver) < 3 else video.get(cv2.CAP_PROP_FPS)
    saveEachFrame = 60*int(fps) / args.n
    print("Frames per second using: {0}".format(fps))

    saveNextAtFrame = 0
    framenum = 0
    while video.isOpened():
        ret, frame = video.read()
        if not ret:
            break

        if framenum == saveNextAtFrame:
            cv2.imwrite(
                os.path.join(args.dir2saveImages, "{}_{:07d}.JPEG".format(nameOfVideoWithoutExt, framenum)),
                frame)
            saveNextAtFrame += saveEachFrame
        framenum += 1
    video.release()
