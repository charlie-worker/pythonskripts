#Имеется разметка xgtf 
#(что такое и как что пишется объясню по запросу).
#Необходимо оставить в ней только те треки,
#которые живут дольше n секунд
#Ну а здесь тебе по факту надо удалить обькеты ,
#которые существуют менее n секунд.

import os
from sys import argv //не используется?
import argparse
import xml.etree.cElementTree as ET
from xml.dom import minidom   //не используется?

ET.register_namespace("", "http://lamp.cfar.umd.edu/viper#")
ET.register_namespace("data", "http://lamp.cfar.umd.edu/viperdata#")

parser = argparse.ArgumentParser(description='Killing low time tracks')
parser.add_argument('workingDir', type=str, help='Dir with .xgtf')
parser.add_argument('long', type=int, help='If object live less than n sek it will be killed')
args = parser.parse_args()

def checkXGTFs(dir):
    supportExts = ('.xgtf')
    for name in os.listdir(dir):
        path = os.path.join(dir, name)
        if os.path.isdir(path):
            checkXGTFs(path)
        elif name.lower().endswith(supportExts):
            tree = ET.ElementTree(file=path)
            killLowTime(tree, path)

def killLowTime(tree, path):
    nameOfData = '{http://lamp.cfar.umd.edu/viperdata#}'
    nameStart = '{http://lamp.cfar.umd.edu/viper#}'
    root = tree.getroot()
    #давай приведем это
    framerate = float(root.find('.//' + nameOfData +
                        'fvalue').attrib.get('value'))
    #и это в более читабельный вид
    objectList = root.findall('.//' + nameStart + 'object')
    for obj in objectList:
        framespan = obj.attrib.get('framespan')
        frames = convert(framespan)
        if frames * framerate < args.long:
            print(frames)
            root[1][0].remove(obj)
    tree.write(path,
        xml_declaration='<?xml version="1.0" encoding="UTF-8"?>')

//не работает для множественных диапазонов. Н-р "1:10 375:721"
def convert(distanse):
    start, stop = map(int, distanse.split(':'))
    return(stop-start)

//а если мне понадобится использовать файл как модуль?
checkXGTFs(args.workingDir)
