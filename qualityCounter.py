 # -*- coding: utf-8 -*-


'''задачка такая: есть такой файл или группа таких файлов
надо бы узнать на каких картинках плохое качество детекций, как пример:
[0] For image '1.mkv_0000178.JPEG':
for class 'Human' targets area is 312, candidates area is 0; matched area is 0
for class 'Car' targets area is 0, candidates area is 0; matched area is 0
надо обратить внимание на targets area и matched area
если t=m это отличное качество чтоб там не было (даже 0)
а так считай качетво как m/t. Если оно ниже порога - то плохо, выше все ок)
если классов несколько то (m1+m2+..mn)/(t1+t2+tn)'''
import os
import argparse
import re

#на выход идёт список вида
#[(way to image, m/t)]
#
#
#

nums = set('1234567890')

def catcher(dirWithFiles):
    Records = fileReader(dirWithFiles)
    #print(Records[0][0])
    sameImage = False
    detectionsList = []
    currentImage = ''
    CTA, CCA, CMA = 0, 0, 0 #current target, candidates and matched areas
    TdivM = 0.0 # target/matched
    for record in Records:
        #while re.search(record[0], 'Images dir'):
        #    record.pop(0)
        imageDir = record[5][16:-1]#нормально тут использовать константы?
        /не согласен с привязкой по строкам, вдруг строку добавим еще одну?) Ответ- нет)
        print(imageDir)
        for line in record:
            #print(line)
            if line.find('image') != -1:
                sameImage = currentImage == line[11:-2]
                /вместо проверки на то же имя картинки можно пользоваться словарем dict.get(key, None)
                /если key не будет найден - вернет ответом None.... ну а дальше сам придумай как это обыграть
                if not sameImage:
                    currentImage = line[line.find("'"):-2]
                    print(currentImage)
                    print(os.path.join(imageDir, currentImage))
                    detectionsList.append((os.path.join(imageDir, currentImage), TdivM))
            if line.find('for class') != -1:
                CTA, CCA, CMA = map(int, re.findall('\d+', line))
                if sameImage:
                    TdivM = TdivM + CMA/CTA
                else:
                    TdivM = CMA/CTA
                /CTA может быть =0, тогда получишь сам знаешь что))
    return(detectionsList)




def fileReader(dirWithFiles):
    waysToFiles = filesCollector(dirWithFiles)
    Records = []
    for way in waysToFiles:
        with open(way, 'r') as f:
            Records.append(f.readlines())
    return(Records)
    #[[lines]]...[lines]]]

def counter(dirWithFiles):
    #waysToFiles = reader(dirWithFiles)
    pass

def filesCollector(dirWithFiles):
    supportedExts = ('.cmp')
    waysToFiles = []
    for name in os.listdir(dirWithFiles):
        path = os.path.join(name)
        if os.path.isdir(path):
            waysToFiles = waysToFiles + filesCollector(path)
        elif name.lower().endswith(supportedExts):
            waysToFiles.append(path)
    return(waysToFiles)


def startAsScript():
    parser = argparse.ArgumentParser(description='Counting a quality of the detection in file')
    parser.add_argument('dirWithFiles', type=str,
         help='Input dir for .cmp files', default= '/home/charlie/xgtf/',)
    parser.add_argument('qualityLine', type=float,
         help='Minimal quality you need', default= 0.5)
    args = parser.parse_args()
    /Сразу же сдесь напишу, что параметры у тебя получилизь позиционными и толку от default никакого (их надо в любом случае вводить)
    
    и у меня сразу же посыпался с ошибкой: FileNotFoundError: [Errno 2] No such file or directory: 'small_dataset_yolov4-ob
    j-DPE400_60000_onnx_thr-1.cmp', когда я запустил так: D:\repo\charlie_worker_pythonskripts>py qualityCounter.py ./cmp 0.5
    
    Следующее: сохрани информацию о том откуда ты взял детекции, так будет информативнее)
               запиши это все в файл, так будет надежнее и удобнее читать выводы
               в целях повышения наглядности /плохие/ и /хорошие/ детекции лучше записать как-нибудь отдельно, 
               а не выводить инфу о каждой картинке /


    print(catcher(args.dirWithFiles))

if __name__ == "__main__":
    startAsScript()